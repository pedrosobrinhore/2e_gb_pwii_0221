import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:"",
    loadChildren:() => import('./components/home/home.module').
    then(m=>m.HomeModule)
  },
  {
    path:"login",
    loadChildren:() => import('./components/login/login.module').
    then(m=>m.LoginModule)
  },
  {
    path:"motos",
    loadChildren:() => import('./components/motos/motos.module').
    then(m=>m.MotosModule)
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
